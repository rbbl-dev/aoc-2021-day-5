import * as fs from 'fs';

fs.readFile("input.txt", (err, data) => {
    if (err) {
        console.error(err)
        return
    }
    let ventLines = getInputData(data.toString())
    let board = buildBoard(ventLines)
    console.log(getOverlapCount(ventLines, board))
})

function getInputData(data: string): VentLine[] {
    let result: VentLine[] = []
    for (let line of data.split("\n")) {
        let points = line.split("->")
        result.push(new VentLine(createPoint(points[0]), createPoint(points[1])))
    }
    return result
}

function createPoint(input: string): Point {
    let values = input.trim().split(",")
    return new Point(Number(values[0]), Number(values[1]))
}

function buildBoard(data: VentLine[]): number[][] {
    let largestXAndY = getLargestXAndY(data)
    let board: number[][] = []
    for (let yIterator = 0; yIterator <= largestXAndY[1]; yIterator++) {
        board.push(Array(largestXAndY[0] + 1).fill(0))
    }
    return board
}

function getLargestXAndY(data: VentLine[]): [number, number] {
    let largestX = 0
    let largestY = 0
    data.forEach(ventLine => {
        if (ventLine.start.x > largestX) {
            largestX = ventLine.start.x
        }
        if (ventLine.start.y > largestY) {
            largestY = ventLine.start.y
        }
        if (ventLine.end.x > largestX) {
            largestX = ventLine.end.x
        }
        if (ventLine.end.y > largestY) {
            largestY = ventLine.end.y
        }
    })
    return [largestX, largestY]
}

function getOverlapCount(ventLines: VentLine[], board: number[][]): number {
    for (let ventLine of ventLines) {
        for (let point of ventLine.getAllPoints()) {
            board[point.y][point.x] += 1
        }
    }
    return calculateGetDangerousAreasNumber(board)
}

function calculateGetDangerousAreasNumber(markerBoard: number[][]): number {
    let count = 0;
    for (let row of markerBoard) {
        for (let element of row) {
            if (element >= 2) {
                count++
            }
        }
    }
    return count
}

class VentLine {
    public lineType: LineType

    constructor(public start: Point, public end: Point) {
        if (start.x == end.x) {
            this.lineType = LineType.VERTICAL
        } else if (start.y == end.y) {
            this.lineType = LineType.HORIZONTAL
        } else {
            this.lineType = LineType.ANGLED
        }
    }

    getAllPoints(): Point[] {
        let xValues = []
        let yValues = []

        if (this.start.x < this.end.x) {
            for (let i = this.start.x; i <= this.end.x; i++) {
                xValues.push(i)
            }
        } else {
            for (let i = this.start.x; i >= this.end.x; i--) {
                xValues.push(i)
            }
        }
        if (this.start.y < this.end.y) {
            for (let i = this.start.y; i <= this.end.y; i++) {
                yValues.push(i)
            }
        } else {
            for (let i = this.start.y; i >= this.end.y; i--) {
                yValues.push(i)
            }
        }

        let allPoints: Point[] = []

        if (this.lineType == LineType.HORIZONTAL) {
            let y = yValues.pop()!
            for (let x of xValues) {
                allPoints.push(new Point(x, y))
            }
        } else if (this.lineType == LineType.VERTICAL) {
            let x = xValues.pop()!
            for (let y of yValues) {
                allPoints.push(new Point(x, y))
            }
        } else {
            for (let i = 0; i < xValues.length; i++) {
                allPoints.push(new Point(xValues[i], yValues[i]))
            }
        }
        return allPoints
    }
}

class Point {
    constructor(public x: number, public y: number) {
    }
}

enum LineType {HORIZONTAL, VERTICAL, ANGLED}